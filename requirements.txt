Django==2.1.2
mysqlclient==1.3.13
pytz==2018.5
transitions==0.6.8
pandas==0.23.4
