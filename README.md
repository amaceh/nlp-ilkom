# NLP ILKOM UPI
Natural Language Processing simple implementation at [https://amaceh.pythonanywhere.com/](https://amaceh.pythonanywhere.com/)


## Contributor
* [Achmad Abdul Rofiq](https://gitlab.com/amaceh) - 1503631
* [Bisma Wahyu Anaafie](https://gitlab.com/BismaWahyu) - 1505066

## Current Implementation
* Morphological Parser (WIP)
* Minimum Edit Distance (WIP)

## How To Use
* create virtual environments
* `pip install -r requirements.txt` to install depedencies
* "db seeder" is not yet exist, just import `db.sql` and '`db2.sql` to mysql. no need for migrations. (future release will be better!)
* copy `opt.cnf.example` and rename it to `opt.cnf`
* set up your database in `opt.cnf`
* then `cd` to project dir and run `python manage.py runserver`
* open [http://localhost:8000/](http://localhost:8000/)


## Coming Soon
* import database using csv/ods/xlxs to make migration useful
* more NLP implementation

