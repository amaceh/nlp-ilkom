from math import log
from django.shortcuts import render
from .models import Bigram, Unigram


def probs_counter(word_list):
    word_list = ['<s>'] + word_list + ['</s>']
    kl_pair = []
    unig_freq = []
    result = []
    kl_len = len(word_list)
    i = 0

    for wd in word_list:
        unig = Unigram.objects.filter(kata__iexact=wd)
        if unig:
            unig_freq.append(int(unig[0].frekuensi))
        else:
            unig_freq.append(1)

    while(i < kl_len-1):
        kl_pair.append(word_list[i+1] + ' | ' + word_list[i])
        freq = Bigram.objects.filter(
            pasangan_kata__iexact=word_list[i] + ' ' + word_list[i+1])
        if freq:
            result.append(log((int(freq[0].frekuensi)+1)/unig_freq[i]))
        else:
            result.append(log(1/unig_freq[i]))
        i = i + 1

    return result, kl_pair


# Create your views here.
def index(request):
    kalimat = request.POST.get('kalimat')
    if kalimat:
        kalimat = kalimat.strip()
        context = {
            'last_value': kalimat
        }
        kalimat = kalimat.lower()

        klmt_list = kalimat.split(' ')
        context['klmt'] = klmt_list
        uni_list = []
        bi_list = []
        bi_prob_list = []
        unig_freq = 0
        for kl in klmt_list:
            unig = Unigram.objects.filter(kata__iexact=kl)
            if unig:
                uni_list.append(unig[0].frekuensi)
                unig_freq = int(unig[0].frekuensi)
            else:
                uni_list.append('1')
                unig_freq = 1
            row_list = []
            row_list2 = []
            for klm in klmt_list:
                freq = Bigram.objects.filter(
                    pasangan_kata__iexact=kl + ' ' + klm)
                if freq:
                    freq_num = freq[0].frekuensi
                    row_list.append(int(freq_num)+1)  # Smoothing
                    row_list2.append((int(freq_num)+1)/unig_freq)  # Smoothing
                else:
                    row_list.append(1)
                    row_list2.append(1/unig_freq)
            bi_list.append([kl] + row_list)
            bi_prob_list.append([kl] + row_list2)

        context['uni'] = uni_list
        context['bi'] = bi_list
        context['bi_prob'] = bi_prob_list
        probs, context['probs_pair'] = probs_counter(klmt_list)
        context['probs'] = probs
        context['probs_total'] = sum(probs) 

        return render(request, 'lang_model/index.html', context)
    return render(request, 'lang_model/index.html')
