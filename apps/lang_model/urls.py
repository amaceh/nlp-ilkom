from django.urls import path

from . import views


app_name = 'lang_model'
urlpatterns = [
    path('', views.index, name='index'),
]