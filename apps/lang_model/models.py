from django.db import models


# Create your models here.
class Bigram(models.Model):
    pasangan_kata = models.CharField(max_length=250)
    frekuensi = models.IntegerField()

    def __str__(self):
        return self.pasangan_kata
    
    class Meta:
        indexes = [
            models.Index(fields=['pasangan_kata', ])
        ]


class Unigram(models.Model):
    kata = models.CharField(max_length=250)
    frekuensi = models.IntegerField()

    def __str__(self):
        return self.kata

    class Meta:
        indexes = [
            models.Index(fields=['kata', ])
        ]
