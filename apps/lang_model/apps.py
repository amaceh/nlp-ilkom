from django.apps import AppConfig


class LangModelConfig(AppConfig):
    name = 'lang_model'
