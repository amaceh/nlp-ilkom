from django.db import models


class Other_Word(models.Model):
    kata = models.CharField(max_length=200)
    ket = models.CharField(max_length=20)
    
    def __str__(self):
        return self.kata

    class Meta:
        indexes = [
            models.Index(fields=['kata', ])
        ]
