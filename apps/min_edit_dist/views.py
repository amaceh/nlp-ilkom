from django.shortcuts import render
import pandas as pd
from apps.morphology_parser.models import Noun, Verb, Adjective
from .models import Other_Word
"""
Pages Functions
"""


def index(request):
    return render(request, 'min_edit_dist/index.html')


def min_edit_table(request):
    kata1 = request.POST.get('kata1')
    kata2 = request.POST.get('kata2')
    if kata1 or kata2:
        kata1 = kata1.strip()
        kata2 = kata2.strip()
        context = {
            'last_value1': kata1,
            'last_value2': kata2
        }
        df, ptr = edit_distance_table(kata1.upper(), kata2.upper())
        clas = [
            "table", "table-responsive",
            "table-bordered", "table-hover"
        ]
        context['df'] = df.to_html(classes=clas, table_id='dataframe').replace(
            'style="text-align: right;"', '')
        row_len = len([row[1] for row in ptr])
        col_len = len(ptr[1])
        context['ptr'] = backtrack(ptr, col_len, row_len)
        return render(request, 'min_edit_dist/min_edit_table.html', context)
    return render(request, 'min_edit_dist/min_edit_table.html')


def close_word(request):
    kata = request.POST.get('kata')
    hasil = []
    if kata:
        words = query_close_word(kata)
        for ws in words:
            ed_dist = levenshtein_iter(kata, ws)
            if ed_dist != -1:
                hasil.append([ws, ed_dist])

        hasil.sort(key=lambda x: x[1])
        context = {
            'hasil': hasil,
            'last_value': kata
        }
        return render(request, 'min_edit_dist/close_word.html', context)
    return render(request, 'min_edit_dist/close_word.html')


"""
Minimum Edit Distance
Functions
"""


def query_close_word(word):
    res = []
    len_word = len(word)
    list_of_word = []
    i = 0
    while i < len_word-1:
        list_of_word.append(word[i]+word[i+1])
        i = i + 1

    list_of_word = set(list_of_word)
    for w in list_of_word:
        res = res + \
            list(Verb.objects.filter(kata_kerja__icontains=w).values_list(
                'kata_kerja', flat=True))
        res = res + \
            list(Adjective.objects.filter(
                kata_sifat__icontains=w).values_list('kata_sifat', flat=True))
        res = res + \
            list(Noun.objects.filter(kata_benda__icontains=w).values_list(
                'kata_benda', flat=True))
        res = res + \
            list(Other_Word.objects.filter(kata__icontains=w).values_list(
                'kata', flat=True))
    return set(res)


# Visual counter levenshtein distance
# Using pandas export html later
def levenshtein_df(dataframe, col_len, row_len, ptr):
    for col in range(1, col_len):
        for row in range(1, row_len):
            if dataframe.index[row] == dataframe.columns[col]:
                cost = 0
            else:
                cost = 2

            cond = [dataframe.iloc[row-1][col] + 1,       # deletion 0, up
                    dataframe.iloc[row][col-1] + 1,       # insertion 1, left
                    dataframe.iloc[row-1][col-1] + cost]  # substitution 2,diag
            dataframe.iloc[row][col] = min(cond)
            ptr[row][col] = min(range(len(cond)), key=cond.__getitem__)
    return ptr


# Counting Only Levenshtein Distance
def levenshtein_iter(a, t):
    rows = len(a)+1
    cols = len(t)+1
    dist = [[0 for x in range(cols)] for x in range(rows)]

    for i in range(1, rows):
        dist[i][0] = i

    for i in range(1, cols):
        dist[0][i] = i

    for col in range(1, cols):
        for row in range(1, rows):
            if a[row-1] == t[col-1]:
                cost = 0
            else:
                cost = 2
            dist[row][col] = min(dist[row-1][col] + 1,       # deletion
                                 dist[row][col-1] + 1,       # insertion
                                 dist[row-1][col-1] + cost)  # substitution
    return dist[row][col]


# initialize dataframe
def edit_distance_table(a, b):
    a = '#' + a
    b = '#' + b
    len_a = len(a)
    len_b = len(b)
    df = pd.DataFrame(index=list(b), columns=list(a))
    # Inisialisasi
    for i in range(len_a):
        df.iloc[0, i] = i
    for j in range(len_b):
        df.iloc[j, 0] = j
    ptr = [[0 for x in range(len_a)] for x in range(len_b)]

    levenshtein_df(df, len_a, len_b, ptr)
    return df, ptr


# Backtarcking function for visual purpose
def backtrack(ptr, col_len, row_len):
    i = col_len - 1
    j = row_len - 1
    itr = 0
    res_idx = []
    while i > 0 and j > 0:
        res_idx.append([i+1, j+1])
        if ptr[j][i] == 0:
            j = j - 1
        elif ptr[j][i] == 1:
            i = i - 1
        else:
            i = i - 1
            j = j - 1
        itr = itr + 1
    # The 0,1,2 rule is broken in first column/row
    # just mark it straight to the initial char
    while i > 0:
        res_idx.append([i, 1])
        i = i - 1
    while j > 0:
        res_idx.append([1, j])
        j = j - 1
    return res_idx
