from django.apps import AppConfig


class MinEditDistConfig(AppConfig):
    name = 'min_edit_dist'
