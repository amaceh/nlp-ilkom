from django.urls import path

from . import views


app_name = 'min_edit_dist'
urlpatterns = [
    path('', views.index, name='index'),
    path('levenshtein_table', views.min_edit_table, name='table'),
    path('find_close_word/', views.close_word, name='close_word'),
]
