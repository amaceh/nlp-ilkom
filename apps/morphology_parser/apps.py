from django.apps import AppConfig


class MorphologyParserConfig(AppConfig):
    name = 'morphology_parser'
