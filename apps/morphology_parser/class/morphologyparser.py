"""
    A class to define Finite State Automata
    for Indonesian Morphology Parsing
"""
from transitions import Machine


class MorphologyParser:

    # the states
    states = [
        'q0',  # start state
        'q1', 'q2', 'q3', 'q4', 'q5', 'q6',  # state prefix
        'q7', 'q8', 'q9', 'q10'  # state n/v/adj
        'q11',  # finish state
        ]

    # List of Transitons, [trigger, source, destination]
    transitions = [
        # Transition triggered by prefixs
        ['meng', 'q0', 'q1'],
        ['me', 'q0', 'q2'],
        ['men', 'q0', 'q3'],
        ['mem', 'q0', 'q4'],
        ['meny', 'q0', 'q5'],
        ['menge', 'q0', 'q6'],
        ['', 'q0', 'q10'],  # epsilon transition 

        # Transistion Triggered by first word after prefix
        # trigger code : 1 == vocal, 0 == consonan,
        # 3 == one syllable. (1 suku kata)
        # _\w_ == "word", else == literal char
        ['1jkhg', 'q1', 'q7'],
        ['_kh_', 'q1', 'q8'],
        ['lmnryw_ng_ny_', 'q2', 'q7'],
        ['cdjt', 'q3', 'q7'],
        ['_sy_', 'q3', 'q9'],
        ['bpf', 'q4', 'q7'],
        ['s', 'q5', 'q10'],
        ['3', 'q6', 'q10'],

        # Transition triggered by words ditionary
        ['verb', 'q7', 'q11'],
        ['adj', 'q8', 'q11'],
        ['noun', 'q9', 'q11'],
        ['all', 'q10', 'q11'],
    ]

    def __init__(self):
        self.machine = Machine(
            model=self, states=self.states,
            transitions=self.transitions, initial='q0'
            )
