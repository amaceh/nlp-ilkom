from django.urls import path

from . import views


app_name = 'morphology_parser'
urlpatterns = [
    path('', views.index, name='index'),
    path('', views.index, name='parse'),
]