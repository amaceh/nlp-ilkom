from django.db import models


# Create your models here.
class Adjective(models.Model):
    kata_sifat = models.CharField(max_length=200)

    def __str__(self):
        return self.kata_sifat

    class Meta:
        indexes = [
            models.Index(fields=['kata_sifat', ])
        ]


class Verb(models.Model):
    kata_kerja = models.CharField(max_length=200)

    def __str__(self):
        return self.kata_kerja

    class Meta:
        indexes = [
            models.Index(fields=['kata_kerja', ])
        ]


class Noun(models.Model):
    kata_benda = models.CharField(max_length=200)

    def __str__(self):
        return self.kata_benda

    class Meta:
        indexes = [
            models.Index(fields=['kata_benda', ])
        ]
