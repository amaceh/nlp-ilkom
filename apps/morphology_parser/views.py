import re
from difflib import SequenceMatcher

from django.shortcuts import render
from .models import Noun, Verb, Adjective


def index(request):
    kata = request.POST.get('kata')
    if kata:
        context = morp_parse(kata)        
        return render(request, 'morphology_parser/index.html', context)
    return render(request, 'morphology_parser/index.html')


def morp_parse(kata):
    kata = kata.strip()
    context = {
        'last_value': kata
    }
    rg = re.findall(r'(menge|meng|meny|men|mem|me)(\w+)', kata)
    if not rg:
        is_verb = Verb.objects.filter(kata_kerja__iexact=kata)
        is_adj = Adjective.objects.filter(kata_sifat__iexact=kata)
        is_noun = Noun.objects.filter(kata_benda__iexact=kata)

        if is_verb:
            context['result'] = 'Pattern Diterima'
            context['word'] = is_verb[0]
            context['ptrn'] = 'V'
        elif is_adj:
            context['result'] = 'Pattern Diterima'
            context['word'] = is_adj[0]
            context['ptrn'] = 'Adj'
        elif is_noun:
            context['result'] = 'Pattern Diterima'
            context['word'] = is_noun[0]
            context['ptrn'] = 'N'
        else:
            context['error_message'] = "Pattern Tidak Diterima."
    else:
        pattern(rg[0], context)
    return context


def pattern(words, context):
    huruf_awal = words[1][0]
    kata_dasar = words[1]
    if words[0] == 'menge':
        verb = Verb.objects.filter(kata_kerja__icontains=kata_dasar)
        is_verb = is_rights(verb, kata_dasar)

        # default false value
        is_adj = -1
        is_noun = -1

        if is_verb > -1:
            context['result'] = 'Pattern Diterima'
            context['word'] = verb[is_verb]
            context['ptrn'] = 'Menge +' + 'V'
        else:
            adj = Adjective.objects.filter(kata_sifat__icontains=kata_dasar)
            is_adj = is_rights(adj, kata_dasar)

            if is_adj > -1:
                context['result'] = 'Pattern Diterima'
                context['word'] = adj[is_adj]
                context['ptrn'] = 'Menge + Adj'
            else:
                noun = Noun.objects.filter(kata_benda__icontains=kata_dasar)
                is_noun = is_rights(noun, kata_dasar)

                if is_noun > -1:
                    context['result'] = 'Pattern Diterima'
                    context['word'] = noun[is_noun]
                    context['ptrn'] = 'Menge + N'
                else:
                    context['error_message'] = "Pattern Tidak Diterima."
        # if there is no match at all
    elif words[0] == 'meng':
        if huruf_awal in 'aiueojkhg':
            verb = Verb.objects.filter(kata_kerja__icontains=kata_dasar)
            is_verb = is_rights(verb, kata_dasar)
            if is_verb > -1:
                context['result'] = 'Pattern Diterima'
                context['word'] = verb[is_verb]
                context['ptrn'] = 'Meng + V'
            else:
                context['error_message'] = "Pattern Tidak Diterima."
        elif kata_dasar[:2] == 'kh':
            adj = Adjective.objects.filter(kata_sifat__icontains=kata_dasar)
            is_adj = is_rights(adj, kata_dasar)
            if is_adj > -1:
                context['result'] = 'Pattern Diterima'
                context['word'] = adj[is_adj]
                context['ptrn'] = 'Meng + Adj'
            else:
                context['error_message'] = "Pattern Tidak Diterima."
        else:
            context['error_message'] = "Pattern Tidak Diterima."
    elif words[0] == 'me':
        if ((huruf_awal in 'lmnryw') or kata_dasar[:2] == 'ng' or
                kata_dasar[:2] == 'ny'):
            verb = Verb.objects.filter(kata_kerja__icontains=kata_dasar)
            is_verb = is_rights(verb, kata_dasar)
            if is_verb > -1:
                context['result'] = 'Pattern Diterima'
                context['word'] = verb[is_verb]
                context['ptrn'] = 'Me + V'
            else:
                context['error_message'] = "Pattern Tidak Diterima."
        else:
            context['error_message'] = "Pattern Tidak Diterima."
    elif words[0] == 'men':
        if huruf_awal in 'cdjt':
            verb = Verb.objects.filter(kata_kerja__icontains=kata_dasar)
            is_verb = is_rights(verb, kata_dasar)
            if is_verb > -1:
                context['result'] = 'Pattern Diterima'
                context['word'] = verb[is_verb]
                context['ptrn'] = 'Men + V'
            else:
                context['error_message'] = "Pattern Tidak Diterima."
        elif kata_dasar[:2] == 'sy':
            noun = Noun.objects.filter(kata_benda__icontains=kata_dasar)
            is_noun = is_rights(noun, kata_dasar)
            if is_noun > -1:
                context['result'] = 'Pattern Diterima'
                context['word'] = noun[is_noun]
                context['ptrn'] = 'Men + N'
            else:
                context['error_message'] = "Pattern Tidak Diterima."
        else:
            context['error_message'] = "Pattern Tidak Diterima."
    elif words[0] == 'mem':
        verb = Verb.objects.filter(kata_kerja__icontains=kata_dasar)
        is_verb = is_rights(verb, kata_dasar)
        if is_verb > -1:
            context['result'] = 'Pattern Diterima'
            context['word'] = verb[is_verb]
            context['ptrn'] = 'Mem + V'
        else:
            context['error_message'] = "Pattern Tidak Diterima."
    elif words[0] == 'meny':
        verb = Verb.objects.filter(kata_kerja__icontains=kata_dasar)
        is_verb = is_rights(verb, kata_dasar)

        # default false value
        is_adj = -1
        is_noun = -1

        if is_verb > -1:
            context['result'] = 'Pattern Diterima'
            context['word'] = verb[is_verb]
            context['ptrn'] = 'Menge +' + 'V'
        else:
            adj = Adjective.objects.filter(kata_sifat__icontains=kata_dasar)
            is_adj = is_rights(adj, kata_dasar)

            if is_adj > -1:
                context['result'] = 'Pattern Diterima'
                context['word'] = adj[is_adj]
                context['ptrn'] = 'Menge + Adj'
            else:
                noun = Noun.objects.filter(kata_benda__icontains=kata_dasar)
                is_noun = is_rights(noun, kata_dasar)

                if is_noun > -1:
                    context['result'] = 'Pattern Diterima'
                    context['word'] = noun[is_noun]
                    context['ptrn'] = 'Menge + N'
                else:
                    context['error_message'] = "Pattern Tidak Diterima."


def is_rights(queryset, desired_word):
    i = 0
    max_match = 0
    max_iter = -1
    for item in queryset:
        match = SequenceMatcher(a=str(
            item), b=desired_word).ratio() > 0.75
        if match:
            if max_match < match:
                max_match = match
                max_iter = i
        i += 1
    return max_iter
