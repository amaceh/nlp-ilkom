import re
import operator
from math import log
from django.shortcuts import render
from apps.lang_model.models import Bigram, Unigram
from apps.morphology_parser.models import Noun, Verb, Adjective
from apps.min_edit_dist.models import Other_Word
from apps.morphology_parser.views import morp_parse


# Create your views here.
def index(request):
    return render(request, 'index/index.html')


def spell(request):
    kalimat = request.POST.get('words')
    if kalimat:
        kalimat = kalimat.strip()
        context = {
            'last_value': kalimat
        }

        kalimat = kalimat.lower()
        klmt_list = kalimat.split(' ')
        context['step0'] = klmt_list
        # Step 1 Cek Tiap Kata dengan Kata di 
        # Dataset dengan lahvenstein distance
        i = 0
        kl_list_2 = []
        for kl in klmt_list:
            # Step 2 Cek Susunan Morphology dengan Morphology Parser
            rg = re.findall(r'(menge|meng|meny|men|mem|me)(\w+)', kl)
            if rg:
                false_contex = morp_parse(kl)
                if 'result' in false_contex:
                    kl_list_2.append([kl, 0])
                else:  # Pakai lehvenstein langsung jika tidak match
                    cl_wrd = close_word(rg[0][1])
                    new_wrd = rg[0][0] + cl_wrd[0]
                    false_contex = morp_parse(new_wrd)
                    if 'result' in false_contex:
                        kl_list_2.append([new_wrd, 0])
                    else:
                        kl_list_2.append(close_word_unig(kl))
            else:
                kl_list_2.append(close_word(kl))
            i += 1
        context['step1'] = kl_list_2
        # Step 3 Cari Alternatif Kata Lain dengan Kombinasi
        prob_word = combiner_lan_model(kl_list_2)
        final_words = []
        for wd in prob_word:
            final_words.append([' '.join(wd), probs_counter(wd)])
        if final_words:
            final_words.sort(key=operator.itemgetter(1), reverse=True)
        context['step2'] = final_words
        # Lalu ambil 5 kata yang menghasilkan peluang tertinggi
        return render(request, 'index/spelling_corr.html', context)
    return render(request, 'index/spelling_corr.html')

'''
Language Model
'''


def combiner_lan_model(word_list):
    word_len = len(word_list)
    combin_list = []
    word_index = 0
    for wd in word_list:
        get_bi = Bigram.objects.filter(
            pasangan_kata__istartswith=wd[0]).order_by('frekuensi')
        i = 0
        for big in get_bi:
            word = []
            for j in range(0, word_index):
                word.append(word_list[j][0])
            word.append(str(big).split(' ')[0])
            word.append(str(big).split(' ')[1])
            for j in range(word_index+2, word_len):
                word.append(word_list[j][0])
            if i > 10:
                break
            i += 1
            combin_list.append(word)
        word_index += 1
    return combin_list


def probs_counter(word_list):
    unig_freq = []
    result = []
    kl_len = len(word_list)
    i = 0

    for wd in word_list:
        unig = Unigram.objects.filter(kata__iexact=wd)
        if unig:
            unig_freq.append(int(unig[0].frekuensi))
        else:
            unig_freq.append(1)

    while(i < kl_len-1):
        freq = Bigram.objects.filter(
            pasangan_kata__iexact=word_list[i] + ' ' + word_list[i+1])
        if freq:
            result.append(log((int(freq[0].frekuensi)+1)/unig_freq[i]))
        else:
            result.append(log(1/unig_freq[i]))
        i = i + 1

    return sum(result)


"""
Min Edit Distance
"""


def query_close_word(word):
    res = []
    len_word = len(word)
    list_of_word = []
    i = 0
    while i < len_word-1:
        list_of_word.append(word[i]+word[i+1])
        i = i + 1

    list_of_word = set(list_of_word)
    for w in list_of_word:
        res = res + \
            list(Verb.objects.filter(kata_kerja__icontains=w).values_list(
                'kata_kerja', flat=True))
        res = res + \
            list(Adjective.objects.filter(
                kata_sifat__icontains=w).values_list('kata_sifat', flat=True))
        res = res + \
            list(Noun.objects.filter(kata_benda__icontains=w).values_list(
                'kata_benda', flat=True))
        res = res + \
            list(Other_Word.objects.filter(kata__icontains=w).values_list(
                'kata', flat=True))
    return set(res)


def query_close_word_unig(word):
    res = []
    len_word = len(word)
    list_of_word = []
    i = 0
    while i < len_word-1:
        list_of_word.append(word[i]+word[i+1])
        i = i + 1

    list_of_word = set(list_of_word)
    for w in list_of_word:
        res = res + \
            list(Unigram.objects.filter(kata__icontains=w).values_list(
                'kata', flat=True))
    return set(res)


def levenshtein_iter(a, t):
    rows = len(a)+1
    cols = len(t)+1
    dist = [[0 for x in range(cols)] for x in range(rows)]

    for i in range(1, rows):
        dist[i][0] = i

    for i in range(1, cols):
        dist[0][i] = i

    for col in range(1, cols):
        for row in range(1, rows):
            if a[row-1] == t[col-1]:
                cost = 0
            else:
                cost = 2
            dist[row][col] = min(dist[row-1][col] + 1,       # deletion
                                 dist[row][col-1] + 1,       # insertion
                                 dist[row-1][col-1] + cost)  # substitution
    return dist[row][col]


def close_word(word):
    words = query_close_word(word)
    hasil = []
    for ws in words:
        ed_dist = levenshtein_iter(word, ws)
        if ed_dist != -1:
            hasil.append([ws, ed_dist])

    hasil.sort(key=lambda x: x[1])
    if hasil:
        return hasil[0]
    return ''


def close_word_unig(word):
    words = query_close_word_unig(word)
    hasil = []
    for ws in words:
        ed_dist = levenshtein_iter(word, ws)
        if ed_dist != -1:
            hasil.append([ws, ed_dist])

    hasil.sort(key=lambda x: x[1])
    if hasil:
        return hasil[0]
    return ''
