from django.urls import path

from . import views


app_name = 'index'
urlpatterns = [
    path('', views.index, name='index'),
    path('spelling_correction', views.spell, name='parse')
]